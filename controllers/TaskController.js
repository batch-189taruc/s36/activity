const Task = require('../models/Task.js')


module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// register some data first

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name})

	return newTask.save().then((savedTask,error) => {
		if(error){
			return error
		}

		return 'Task created successfully!'
	})
}


//FINDING SPECIFIC TASK
module.exports.findTask = (taskId) =>{
	return Task.findById(taskId).then((foundTask, error) => {
		if(error){
			return error
		}
		return foundTask
	})
}

//CHANGING STATUS

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}
		result.status = newContent.status
		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}

			return updatedTask
		})
	})
}