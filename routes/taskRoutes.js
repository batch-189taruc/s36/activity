const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()


router.get('/',(req,res) => {
	//business logic here
	TaskController.getAllTasks().then((tasks) => res.send(tasks))

})
// here, we will post some data first

router.post('/register', (req,res) => {
	TaskController.createTask(req.body).then((task) => res.send(task))
})

//FINDING SPECIFIC TASK
router.get('/:id', (req,res) =>{
	TaskController.findTask(req.params.id).then((foundTask) => res.send(foundTask))
})
module.exports = router


//CHANGING STATUS

router.put('/:id/complete', (req,res) =>{
	TaskController.updateTask(req.params.id, req.body).then((updatedTask) => 
		res.send(updatedTask))
})